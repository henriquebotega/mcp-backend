## Rodar o projeto

npm start

## Build do projeto

npm run build

## Deploy

Ao empurar para o GitLab, a pipeline vai disparar o deployment para a Vercel/Heroku

Neste momento a Heroku precisa pagar $5 mensal para reativação.

## Variaveis de ambiente

Gitlab -> mcp-backend -> Settings -> CI/CD -> Variables

https://gitlab.com/henriquebotega/mcp-backend/-/settings/ci_cd

