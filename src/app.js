require("dotenv").config();

const express = require("express");
const cors = require("cors");
const helmet = require("helmet");
const mongoose = require("mongoose");

mongoose.connect(process.env.MONGODB_URL, {
	useCreateIndex: true,
	useNewUrlParser: true,
	useUnifiedTopology: true,
	useFindAndModify: false,
});

const db = mongoose.connection;

db.on("error", console.error.bind(console, "connection error: "));

db.once("open", function () {
  console.log("Connected successfully");
});

const app = express();

// app.disable("etag");
app.use(cors());
app.use(helmet());
app.use(express.json({ limit: "50mb" }));
app.use(express.urlencoded({ extended: true, limit: "50mb", parameterLimit: 50000 }));

require("./models/About");
require("./models/Banner");
require("./models/Category");
require("./models/Contact");
require("./models/Config");
require("./models/Event");
require("./models/EventGallery");
require("./models/Ministry");
require("./models/Newsletter");
require("./models/Product");
require("./models/Prayer");
require("./models/Team");
require("./models/Testimony");
require("./models/User");
require("./models/Customer");
require("./models/OrderStatus");
require("./models/OrderProduct");
require("./models/Order");
require("./models/PagSeguroNotification");

app.use("/api", require("./routes"));

app.listen(process.env.PORT || 3333);

module.exports = app
