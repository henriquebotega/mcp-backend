const mongoose = require("mongoose");
const Order = mongoose.model("Order");
const OrderProduct = mongoose.model("OrderProduct");

const path = require("path");
const { addFileFTP, removeFileFTP, removeFile } = require("../config/jsftp");

const currentPage = "order_product";

const all = async (req, _idCustomer = null) => {
	const { pg = 0, qtd = 10 } = req.query;
	const _idOrder = req.params.order_id;

	let filtro = { _idOrder };

	if (_idCustomer) {
		const order = await Order.find({ _idCustomer });

		if (order.length === 0) {
			return { registrosFiltrados: [], registros: [] };
		}
	}

	const registrosFiltrados = await OrderProduct.find(filtro)
		.limit(Number(qtd))
		.skip(Number(qtd) * Number(pg))
		.sort({ createdAt: -1 })
		.populate("_idOrder")
		.populate("_idProduct");

	const registros = await OrderProduct.find(filtro);

	return { registrosFiltrados, registros };
};

module.exports = {
	async getAllWeb(req, res) {
		const _idCustomer = req.customer._id;
		const { registrosFiltrados, registros } = await all(req, _idCustomer);
		return res.send({ items: registrosFiltrados, total: registros.length });
	},

	async getAll(req, res) {
		const { registrosFiltrados, registros } = await all(req);
		return res.send({ items: registrosFiltrados, total: registros.length });
	},

	async getByID(req, res) {
		const _idOrder = req.params.order_id;

		const registro = await OrderProduct.find({
			_id: req.params.id,
			_idOrder,
		})
			.populate("_idOrder")
			.populate("_idProduct");

		return res.send(registro[0]);
	},

	async incluir(req, res) {
		const { qtd, unit, _idProduct } = req.body;

		const _idOrder = req.params.order_id;

		const body = {
			qtd,
			unit,
			_idProduct,
			_idOrder,
			total: parseFloat((qtd * unit).toFixed(2)),
		};

		if (req.file) {
			body.image = req.file.filename;
			await addFileFTP(currentPage, body.image);
		}

		const newRegistro = await OrderProduct.create(body);
		const registro = await OrderProduct.findById(newRegistro._id).populate(
			"_idOrder"
		);

		return res.send(registro);
	},

	async editar(req, res) {
		const { qtd, unit, _idProduct } = req.body;

		const _idOrder = req.params.order_id;

		const body = {
			qtd,
			unit,
			_idProduct,
			_idOrder,
			total: parseFloat((qtd * unit).toFixed(2)),
		};

		if (req.file) {
			const oldRegistro = await OrderProduct.find({
				_id: req.params.id,
				_idOrder,
			});

			if (oldRegistro[0].image) {
				await removeFile(oldRegistro[0].image);
				await removeFileFTP(currentPage, oldRegistro[0].image);
			}

			body.image = req.file.filename;
			await addFileFTP(currentPage, body.image);
		}

		const newRegistro = await OrderProduct.findByIdAndUpdate(
			req.params.id,
			body,
			{
				new: true,
			}
		);

		const registro = await OrderProduct.findById(newRegistro._id).populate(
			"_idOrder"
		);

		return res.send(registro);
	},

	async excluir(req, res) {
		const registro = await OrderProduct.find({
			_id: req.params.id,
			_idOrder: req.params.order_id,
		});

		if (registro[0].image) {
			await removeFile(registro[0].image);
			await removeFileFTP(currentPage, registro[0].image);
		}

		await OrderProduct.findByIdAndRemove(req.params.id);
		return res.send();
	},
};
