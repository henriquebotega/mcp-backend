const mongoose = require("mongoose");
const Order = mongoose.model("Order");

const OrderProduct = mongoose.model("OrderProduct");

const all = async (req, _idCustomer = null) => {
	const { pg = 0, qtd = 10 } = req.query;

	let filtro = {};

	if (_idCustomer) {
		filtro = { ...filtro, _idCustomer };
	}

	const regFiltrados = await Order.find(filtro)
		.limit(Number(qtd))
		.skip(Number(qtd) * Number(pg))
		.sort({ createdAt: -1 })
		.populate("_idCustomer")
		.populate("_idOrderStatus");

	const registros = await Order.find(filtro);

	let colRegistros = [];
	return new Promise(async (resolve, reject) => {
		for (var i = 0; i < regFiltrados.length; i++) {
			const colProducts = await OrderProduct.find({
				_idOrder: regFiltrados[i]._id,
			}).populate("_idProduct");

			colRegistros.push({
				...regFiltrados[i]._doc,
				items: colProducts,
			});
		}

		resolve({ registrosFiltrados: colRegistros, registros });
	});
};

module.exports = {
	async getAllWeb(req, res) {
		const _idCustomer = req.customer._id;
		const { registrosFiltrados, registros } = await all(req, _idCustomer);
		return res.send({ items: registrosFiltrados, total: registros.length });
	},

	async getAll(req, res) {
		const { registrosFiltrados, registros } = await all(req);
		return res.send({ items: registrosFiltrados, total: registros.length });
	},

	async getAllByProduct(req, res) {
		const registro = await OrderProduct.find({
			_idProduct: req.params.product_id,
		});
		return res.send(registro);
	},

	async getAllByCustomer(req, res) {
		const registro = await Order.find({ _idCustomer: req.params.customer_id });
		return res.send(registro);
	},

	async cancelByIDByCustomer(req, res) {
		const _idCustomer = req.customer._id;
		const _id = req.params.id;

		let registro = await Order.find({ _id, _idCustomer })
			.populate("_idCustomer")
			.populate("_idOrderStatus");

		if (registro.length === 0) {
			return res.send();
		}

		const body = {
			...registro[0]._doc,
			_idOrderStatus: "5fd66ad68ebae834975faa3c", // Cancelada
		};

		const newRegistro = await Order.findByIdAndUpdate(registro[0]._id, body, {
			new: true,
		});

		registro = await Order.findById(newRegistro._id)
			.populate("_idCustomer")
			.populate("_idOrderStatus");

		const colProducts = await OrderProduct.find({
			_idOrder: registro._id,
		}).populate("_idProduct");

		const colRegistros = {
			...registro._doc,
			items: colProducts,
		};

		return res.send(colRegistros);
	},

	async getByIDByCustomer(req, res) {
		const _idCustomer = req.customer._id;
		const _id = req.params.id;

		const registro = await Order.find({ _id, _idCustomer })
			.populate("_idCustomer")
			.populate("_idOrderStatus");

		const colProducts = await OrderProduct.find({
			_idOrder: registro[0]._id,
		}).populate("_idProduct");

		const colRegistros = {
			...registro[0]._doc,
			items: colProducts,
		};

		return res.send(colRegistros);
	},

	async getByID(req, res) {
		const registro = await Order.findById(req.params.id)
			.populate("_idCustomer")
			.populate("_idOrderStatus");

		const colProducts = await OrderProduct.find({
			_idOrder: registro._id,
		}).populate("_idProduct");

		const colRegistros = {
			...registro._doc,
			items: colProducts,
		};

		return res.send(colRegistros);
	},

	async incluir(req, res) {
		const newRegistro = await Order.create(req.body);

		const registro = await Order.findById(newRegistro._id)
			.populate("_idCustomer")
			.populate("_idOrderStatus");

		return res.send(registro);
	},

	async editar(req, res) {
		const newRegistro = await Order.findByIdAndUpdate(req.params.id, req.body, {
			new: true,
		});

		const registro = await OrderProduct.findById(newRegistro._id)
			.populate("_idCustomer")
			.populate("_idOrderStatus");

		return res.send(registro);
	},

	async excluir(req, res) {
		await Order.findByIdAndRemove(req.params.id);
		return res.send();
	},
};
