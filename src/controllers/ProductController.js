const mongoose = require("mongoose");
const Product = mongoose.model("Product");

const path = require("path");
const { addFileFTP, removeFileFTP, removeFile } = require("../config/jsftp");

const currentPage = "product";

const all = async (req, active = false) => {
	const { pg = 0, qtd = 10 } = req.query;

	const filtro = active ? { active: true } : {};

	const registrosFiltrados = await Product.find(filtro)
		.limit(Number(qtd))
		.skip(Number(qtd) * Number(pg))
		.sort({ title: 1 });
		// .sort({ createdAt: 1 });

	const registros = await Product.find(filtro);
	return { registrosFiltrados, registros };
};

module.exports = {
	async getAllWeb(req, res) {
		const { registrosFiltrados, registros } = await all(req, true);
		return res.send({ items: registrosFiltrados, total: registros.length });
	},

	async getAll(req, res) {
		const { registrosFiltrados, registros } = await all(req);
		return res.send({ items: registrosFiltrados, total: registros.length });
	},

	async getByID(req, res) {
		const registro = await Product.findById(req.params.id);
		return res.send(registro);
	},

	async incluir(req, res) {
		const { title, description, value, active, is_virtual, disable_shopping_cart, width, height, length, weight } = req.body;

		const body = {
			title,
			description,
			value,
			active,
			is_virtual,
			disable_shopping_cart,
			width,
			height,
			length,
			weight,
			download: "",
		};

		if (req.files) {
			if (req.files.file && req.files.file.length > 0) {
				body.image = req.files.file[0].filename;
				await addFileFTP(currentPage, body.image);
			}

			if (req.files.file_download && req.files.file_download.length > 0) {
				body.download = req.files.file_download[0].filename;
				await addFileFTP(currentPage, body.download);
			}
		}

		const registro = await Product.create(body);
		return res.send(registro);
	},

	async editar(req, res) {
		const { title, description, value, active, is_virtual, disable_shopping_cart, width, height, length, weight } = req.body;

		const body = {
			title,
			description,
			value,
			active,
			is_virtual,
			disable_shopping_cart,
			width,
			height,
			length,
			weight,
		};

		if (req.files) {
			if (req.files.file && req.files.file.length > 0) {
				const oldRegistro = await Product.findById(req.params.id);

				if (oldRegistro && oldRegistro.image) {
					await removeFile(oldRegistro.image);
					await removeFileFTP(currentPage, oldRegistro.image);
				}

				body.image = req.files.file[0].filename;
				await addFileFTP(currentPage, body.image);
			}

			if (req.files.file_download && req.files.file_download.length > 0) {
				const oldRegistro = await Product.findById(req.params.id);

				if (oldRegistro && oldRegistro.download) {
					await removeFile(oldRegistro.download);
					await removeFileFTP(currentPage, oldRegistro.download);
				}

				body.download = req.files.file_download[0].filename;
				await addFileFTP(currentPage, body.download);
			}
		}

		const registro = await Product.findByIdAndUpdate(req.params.id, body, {
			new: true,
		});

		return res.send(registro);
	},

	async excluir(req, res) {
		const registro = await Product.findById(req.params.id);

		if (registro && registro.image) {
			await removeFile(registro.image);
			await removeFileFTP(currentPage, registro.image);
		}

		await Product.findByIdAndRemove(req.params.id);
		return res.send();
	},
};
