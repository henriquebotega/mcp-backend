const mongoose = require("mongoose");
const parser = require("xml2js").parseString;
const axios = require("axios");

const { sendEmail, converterData, avisarSobreErro } = require("../util");

const PagSeguroNotification = mongoose.model("PagSeguroNotification");
const Product = mongoose.model("Product");
const Config = mongoose.model("Config");

const Order = mongoose.model("Order");
const OrderStatus = mongoose.model("OrderStatus");
const OrderProduct = mongoose.model("OrderProduct");

async function getConfig() {
	const registroConfig = await Config.find({}).limit(1);
	return registroConfig[0];
}

function xmlBoleto(pedido, hash, retornoIBGE) {
	const { cliente, produtos, endereco } = pedido;
	const tel = cliente.tel.replace("-", "");
	const cpf = cliente.cpf.replace(/[\.-]/g, "");

	const xml = `<?xml version='1.0' encoding='UTF-8'?>
		<payment>
			<mode>default</mode>
			<method>boleto</method>
			<sender>
				<name>${cliente.nome}</name>
				<email>${cliente.email}</email>
				<phone>
					<areaCode>${cliente.ddd}</areaCode>
					<number>${tel}</number>
				</phone>
				<documents>
					<document>
						<type>CPF</type>
						<value>${cpf}</value>
					</document>
				</documents>
				<hash>${hash}</hash>
			</sender>
			<currency>BRL</currency>
			<notificationURL>https://mcp-backend.herokuapp.com/api/web/pagseguro/notificacao</notificationURL>
			<items>
				${produtos
					.filter((p) => p.item.value > 0)
					.map((p) => {
						return `<item>
							<id>${p.item.id}</id>
							<description>${p.item.title}</description>
							<quantity>${p.qtd}</quantity>
							<amount>${p.item.value.toFixed(2)}</amount>
						</item>`;
					})}
			</items>
			<extraAmount>0.00</extraAmount>
			<reference>${pedido.id}</reference>
			<shipping>
			<addressRequired>true</addressRequired>
			<address>
				<street>${endereco.logradouro}</street>
				<number>${endereco.numero}</number>
				<complement>${endereco.complemento}</complement>
				<district>${endereco.bairro}</district>
				<city>${retornoIBGE.nome}</city>
				<state>${endereco.estado}</state>
				<country>BRA</country>
				<postalCode>${endereco.cep.replace(/[\.-]/g, "")}</postalCode>
			</address>
			<type>3</type>
			<cost>${pedido.frete.toFixed(2)}</cost>
			</shipping>
		</payment>		
	`;

	return xml;
}

function xmlCartaoCredito(pedido, hash, cartao, parcela, retornoIBGE) {
	const { cliente, produtos, endereco } = pedido;
	const tel = cliente.tel.replace("-", "");
	const cpf = cliente.cpf.replace(/[\.-]/g, "");
	const cartaoCpf = cartao.cpf.replace(/[\.-]/g, "");

	const xml = `<?xml version='1.0' encoding='UTF-8'?>
		<payment>
			<mode>default</mode>
			<method>creditCard</method>
			<sender>
				<name>${cliente.nome}</name>
				<email>${cliente.email}</email>
				<phone>
					<areaCode>${cliente.ddd}</areaCode>
					<number>${tel}</number>
				</phone>
				<documents>
					<document>
						<type>CPF</type>
						<value>${cpf}</value>
					</document>
				</documents>
				<hash>${hash}</hash>
			</sender>
			<currency>BRL</currency>
			<notificationURL>https://mcp-backend.herokuapp.com/api/web/pagseguro/notificacao</notificationURL>
			<items>
				${produtos
					.filter((p) => p.item.value > 0)
					.map((p) => {
						return `<item>
							<id>${p.item.id}</id>
							<description>${p.item.title}</description>
							<quantity>${p.qtd}</quantity>
							<amount>${p.item.value.toFixed(2)}</amount>
						</item>`;
					})}
			</items>
			<extraAmount>0.00</extraAmount>
			<reference>${pedido.id}</reference>
			<shipping>
			<addressRequired>true</addressRequired>
			<address>
				<street>${endereco.logradouro}</street>
				<number>${endereco.numero}</number>
				<complement>${endereco.complemento}</complement>
				<district>${endereco.bairro}</district>
				<city>${retornoIBGE.nome}</city>
				<state>${endereco.estado}</state>
				<country>BRA</country>
				<postalCode>${endereco.cep.replace(/[\.-]/g, "")}</postalCode>
			</address>
			<type>3</type>
			<cost>${pedido.frete.toFixed(2)}</cost>
			</shipping>
			<creditCard>
				<token>${cartao.token}</token>
				<installment>
					<quantity>${parcela.quantity}</quantity>
					<value>${parcela.installmentAmount.toFixed(2)}</value>
				</installment>
				<holder>
					<name>${cartao.name}</name>
					<documents>
						<document>
							<type>CPF</type>
							<value>${cartaoCpf}</value>
						</document>
					</documents>
					<birthDate>${converterData(cartao.dtNasc)}</birthDate>
					<phone>
						<areaCode>${cliente.ddd}</areaCode>
						<number>${tel}</number>
					</phone>
				</holder>
				<billingAddress>
					<street>${endereco.logradouro}</street>
					<number>${endereco.numero}</number>
					<complement>${endereco.complemento}</complement>
					<district>${endereco.bairro}</district>
					<city>${retornoIBGE.nome}</city>
					<state>${endereco.estado}</state>
					<country>BRA</country>
					<postalCode>${endereco.cep.replace(/[\.-]/g, "")}</postalCode>
				</billingAddress>
			</creditCard>
		</payment>
	`;

	return xml;
}

function xmlCartaoDebito(pedido, hash, banco, retornoIBGE) {
	const { cliente, produtos, endereco } = pedido;
	const tel = cliente.tel.replace("-", "");
	const cpf = cliente.cpf.replace(/[\.-]/g, "");

	const xml = `<?xml version='1.0' encoding='UTF-8'?>
		<payment>
			<mode>default</mode>
			<method>eft</method>
			<bank>
			 	<name>${banco.toLowerCase()}</name>
			</bank>
			<sender>
				<name>${cliente.nome}</name>
				<email>${cliente.email}</email>
				<phone>
					<areaCode>${cliente.ddd}</areaCode>
					<number>${tel}</number>
				</phone>
				<documents>
					<document>
						<type>CPF</type>
						<value>${cpf}</value>
					</document>
				</documents>
				<hash>${hash}</hash>
			</sender>
			<currency>BRL</currency>
			<notificationURL>https://mcp-backend.herokuapp.com/api/web/pagseguro/notificacao</notificationURL>
			<items>
				${produtos
					.filter((p) => p.item.value > 0)
					.map((p) => {
						return `<item>
							<id>${p.item.id}</id>
							<description>${p.item.title}</description>
							<quantity>${p.qtd}</quantity>
							<amount>${p.item.value.toFixed(2)}</amount>
						</item>`;
					})}
			</items>
			<extraAmount>0.00</extraAmount>
			<reference>${pedido.id}</reference>
			<shipping>
			<addressRequired>true</addressRequired>
			<address>
				<street>${endereco.logradouro}</street>
				<number>${endereco.numero}</number>
				<complement>${endereco.complemento}</complement>
				<district>${endereco.bairro}</district>
				<city>${retornoIBGE.nome}</city>
				<state>${endereco.estado}</state>
				<country>BRA</country>
				<postalCode>${endereco.cep.replace(/[\.-]/g, "")}</postalCode>
			</address>
			<type>3</type>
			<cost>${pedido.frete.toFixed(2)}</cost>
			</shipping>
		</payment>
	`;

	return xml;
}

const postWeb = async (result, pedido) => {
	const _idCustomer = pedido.cliente._id;

	let registroOS = null;

	if (result) {
		registroOS = await OrderStatus.find({ code: 1 }); // Aguardando pagamento
	} else {
		registroOS = await OrderStatus.find({ code: 99 }); // Gratuito
	}

	let _idOrderStatus = registroOS[0]._id;

	const body = {
		frete: pedido.frete,
		reference: pedido.id,
		address_postal_code: pedido.endereco.cep,
		address_street: pedido.endereco.logradouro,
		address_number: pedido.endereco.numero,
		address_complement: pedido.endereco.complemento,
		address_district: pedido.endereco.bairro,
		address_state: pedido.endereco.estado,
		address_city: pedido.endereco.cidade,
		address_email: pedido.endereco.email,
		_idCustomer,
		_idOrderStatus,
	};

	const newRegistro = await Order.create(body);

	// Adiciona os produtos ao pedido
	const retPromises = await pedido.produtos.map(async (p) => {
		const bodyOP = {
			...p,
			_idOrder: newRegistro._id,
			unit: p.item.value,
			_idProduct: p.item._id,
			total: parseFloat((p.qtd * p.item.value).toFixed(2)),
		};

		return await OrderProduct.create(bodyOP);
	});

	Promise.all(retPromises).then(async () => {
		await Order.findById(newRegistro._id).populate("_idCustomer").populate("_idOrderStatus");
	});
};

module.exports = {
	async calcularFrete(req, res) {
		const { cep_origem_frete } = await getConfig();

		const { produtos } = req.body;
		let height = 0;
		let length = 0;
		let weight = 0;
		let width = 0;
		let total = 0;

		for (let i = 0; i < produtos.length; i++) {
			const registro = await Product.findById(produtos[i]._id);

			height += parseInt(registro.height * produtos[i].unit);
			length += parseInt(registro.length * produtos[i].unit);
			weight += parseFloat((registro.weight * produtos[i].unit).toFixed(2));
			width += parseInt(registro.width * produtos[i].unit);
			total += parseFloat((registro.value * produtos[i].unit).toFixed(2));

			weight = weight / 1000;
		}

		const cepTo = req.body.cep.replace(/[\.-]/g, "");
		const cepFrom = cep_origem_frete.replace(/[\.-]/g, "");

		try {
			const urlCalculo = `https://melhorenvio.com.br/api/v2/calculator?from=${cepFrom}&to=${cepTo}&width=${width}&weight=${weight}&height=${height}&length=${length}&insurance_value=${total}&services=1,2`;

			const { data } = await axios.get(urlCalculo);

			return res.json(data);
		} catch (e) {
			avisarSobreErro(req.body, e);
			return res.json(e);
		}
	},
	async getSession(req, res) {
		try {
			const { email_pagseguro, token_pagseguro } = await getConfig();

			const { data } = await axios({
				method: "POST",
				url: `https://ws.pagseguro.uol.com.br/v2/sessions?email=${email_pagseguro}&token=${token_pagseguro}`,
			});

			parser(data, function (err, result) {
				return res.json({ session_id: result.session.id[0] });
			});
		} catch (e) {
			return res.json(e);
		}
	},
	async notificacao(req, res) {
		const { email_pagseguro, token_pagseguro } = await getConfig();
		const { notificationCode } = req.body;

		await PagSeguroNotification.create(req.body);

		try {
			const retorno = await axios.get(`https://ws.pagseguro.uol.com.br/v3/transactions/notifications/${notificationCode}?email=${email_pagseguro}&token=${token_pagseguro}`);

			parser(retorno.data, async function (err, result) {
				const { transaction } = result;
				const { reference, status } = transaction;
				const codStatus = parseInt(status[0]);

				const currentOrder = await Order.find({
					reference: reference[0],
				})
					.populate("_idCustomer")
					.populate("_idOrderStatus");

				if (currentOrder[0]._idOrderStatus.code !== codStatus) {
					const registroOS = await OrderStatus.find({ code: codStatus });

					const body = {
						...currentOrder[0]._doc,
						_idOrderStatus: registroOS[0]._id,
					};

					const newRegistro = await Order.findByIdAndUpdate(currentOrder[0]._id, body, {
						new: true,
					})
						.populate("_idCustomer")
						.populate("_idOrderStatus");

					return res.send(newRegistro);
				}

				return res.json(currentOrder[0]);
			});
		} catch (e) {
			avisarSobreErro(req.body, e);
			return res.json(e);
		}
	},
	async setPedido(req, res) {
		const { email_pagseguro, token_pagseguro } = await getConfig();
		const { hash, parcela, cartao, banco, pedido, tipoFormaPgto } = req.body;

		// Pedido sem valor (gratuito), apenas registro local
		if (pedido.total + pedido.frete === 0) {
			await postWeb(null, pedido);

			const subject = "Pedido realizado com sucesso - Mulher com Palavra";

			const html = `
			<html>
				<body>
					<h1>Olá ${pedido.cliente.nome}</h1>

					Seu pedido foi registrado, em breve você receberá seu produto.<br />

					<p>Atenciosamente, Equipe Mulher com Palavra.</p>
				</body>
			</html>`;

			await sendEmail({ to: `${pedido.cliente.email}`, subject, html });

			if (pedido.endereco.email) {
				await sendEmail({ to: `${pedido.endereco.email}`, subject, html });
			}

			return res.status(200).send({ gratuito: true });
		}

		let xml;

		const { data: retornoIBGE } = await axios.get(`https://servicodados.ibge.gov.br/api/v1/localidades/municipios/${pedido.endereco.cidade}`);

		if (tipoFormaPgto === "boleto") {
			xml = xmlBoleto(pedido, hash, retornoIBGE);
		}

		if (tipoFormaPgto === "cartao_credito") {
			xml = xmlCartaoCredito(pedido, hash, cartao, parcela, retornoIBGE);
		}

		if (tipoFormaPgto === "debito_online") {
			xml = xmlCartaoDebito(pedido, hash, banco, retornoIBGE);
		}

		try {
			const retorno = await axios.post(`https://ws.pagseguro.uol.com.br/v2/transactions/?email=${email_pagseguro}&token=${token_pagseguro}`, xml, {
				headers: { "Content-Type": "application/xml; charset=UTF-8" },
			});

			parser(retorno.data, async function (err, result) {
				await postWeb(result, pedido);

				const subject = "Pedido realizado com sucesso - Mulher com Palavra";
				let html;

				if (tipoFormaPgto === "boleto") {
					html = `
					<html>
						<body>
							<h1>Olá ${pedido.cliente.nome}</h1>
							<a href='${result.transaction.paymentLink[0]}'>Clique aqui para visualizar o seu boleto</a><br />
							Após a confirmação do pagamento, você receberá seu produto.<br />
							<p>Atenciosamente, Equipe Mulher com Palavra.</p>
						</body>
					</html>`;
				} else {
					html = `
					<html>
						<body>
							<h1>Olá ${pedido.cliente.nome}</h1>
							Após a confirmação do pagamento, você receberá seu produto.<br />
							<p>Atenciosamente, Equipe Mulher com Palavra.</p>
						</body>
					</html>`;
				}

				if (pedido.endereco.email) {
					await sendEmail({ to: `${pedido.endereco.email}`, subject, html });
				} else {
					await sendEmail({ to: `${pedido.cliente.email}`, subject, html });
				}

				return res.json(result);
			});
		} catch (e) {
			avisarSobreErro(req.body, e);
			return res.json(e);
		}
	},
	async cancelarPedido(req, res) {
		const { email_pagseguro, token_pagseguro } = await getConfig();
		const { notificationCode } = req.body;

		try {
			const { data } = await axios({
				method: "POST",
				url: `https://ws.pagseguro.uol.com.br/v2/transactions/cancels?email=${email_pagseguro}&token=${token_pagseguro}&transactionCode=${notificationCode}`,
			});

			parser(data, function (err, result) {
				return res.json({ result: result.result });
			});
		} catch (e) {
			return res.json({ result: "OK" });
		}
	},
	async estornarPedido(req, res) {
		const { email_pagseguro, token_pagseguro } = await getConfig();
		const { notificationCode } = req.body;

		try {
			const { data } = await axios({
				method: "POST",
				url: `https://ws.pagseguro.uol.com.br/v2/transactions/refunds?email=${email_pagseguro}&token=${token_pagseguro}&transactionCode=${notificationCode}`,
			});

			parser(data, function (err, result) {
				return res.json({ result: result.result });
			});
		} catch (e) {
			return res.json({ result: "OK" });
		}
	},
};
