const mongoose = require("mongoose");
const OrderStatus = mongoose.model("OrderStatus");

module.exports = {
	async getAll(req, res) {
		const { pg = 0, qtd = 10 } = req.query;

		const registrosFiltrados = await OrderStatus.find({})
			.limit(Number(qtd))
			.skip(Number(qtd) * Number(pg))
			.sort({ createdAt: -1 });

		const registros = await OrderStatus.find({});

		return res.send({ items: registrosFiltrados, total: registros.length });
	},

	async getByID(req, res) {
		const registro = await OrderStatus.findById(req.params.id);
		return res.send(registro);
	},

	async incluir(req, res) {
		const registro = await OrderStatus.create(req.body);
		return res.send(registro);
	},

	async editar(req, res) {
		const registro = await OrderStatus.findByIdAndUpdate(
			req.params.id,
			req.body,
			{
				new: true,
			}
		);
		return res.send(registro);
	},

	async excluir(req, res) {
		await OrderStatus.findByIdAndRemove(req.params.id);
		return res.send();
	},
};
