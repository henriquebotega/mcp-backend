const mongoose = require("mongoose");
const Ministry = mongoose.model("Ministry");

const path = require("path");
const { addFileFTP, removeFileFTP, removeFile } = require("../config/jsftp");

const currentPage = "ministry";

const all = async (req, active = false) => {
	const { pg = 0, qtd = 10 } = req.query;

	const filtro = active ? { active: true } : {};

	const registrosFiltrados = await Ministry.find(filtro)
		.limit(Number(qtd))
		.skip(Number(qtd) * Number(pg))
		.sort({ createdAt: -1 })
		.populate("_idCategory");

	const registros = await Ministry.find(filtro);

	return { registrosFiltrados, registros };
};

module.exports = {
	async getAllWeb(req, res) {
		const { registrosFiltrados, registros } = await all(req, true);
		return res.send({ items: registrosFiltrados, total: registros.length });
	},

	async getAll(req, res) {
		const { registrosFiltrados, registros } = await all(req);
		return res.send({ items: registrosFiltrados, total: registros.length });
	},

	async postFilter(req, res) {
		const { pg = 0, qtd = 10 } = req.query;
		const filter = req.body.filter;

		let where = { active: true };
		if (filter === "video") {
			where = { ...where, video: { $ne: "" } };
		} else if (filter === "notvideo") {
			where = { ...where, video: { $not: { $ne: "" } } };
		}

		const registros = await Ministry.find(where)
			.limit(Number(qtd))
			.skip(Number(qtd) * Number(pg))
			.sort({ createdAt: -1 })
			.populate("_idCategory");

		return res.send({ items: registros, total: registros.length });
	},

	async getAllByCategory(req, res) {
		const { qtd = 10 } = req.query;
		const _idCategory = req.params.category_id;

		const registros = await Ministry.find({
			_idCategory,
		})
			.limit(Number(qtd))
			.sort({ createdAt: -1 })
			.populate("_idCategory");

		return res.send({ items: registros, total: registros.length });
	},

	async getAllWebByCategory(req, res) {
		const registro = await Ministry.find({
			_idCategory: req.params.category_id,
		});
		return res.send(registro);
	},

	async getByID(req, res) {
		const registro = await Ministry.findById(req.params.id).populate(
			"_idCategory"
		);
		return res.send(registro);
	},

	async incluir(req, res) {
		const { title, description, video, _idCategory, active } = req.body;

		const body = {
			title,
			description,
			video,
			_idCategory,
			active,
		};

		if (req.file) {
			body.image = req.file.filename;
			await addFileFTP(currentPage, body.image);
		}

		const newRegistro = await Ministry.create(body);

		const registro = await Ministry.findById(newRegistro._id).populate(
			"_idCategory"
		);
		return res.send(registro);
	},

	async editar(req, res) {
		const { title, description, video, _idCategory, active } = req.body;

		const body = {
			title,
			description,
			video,
			_idCategory,
			active,
		};

		if (req.file) {
			const oldRegistro = await Ministry.findById(req.params.id);

			if (oldRegistro && oldRegistro.image) {
				await removeFile(oldRegistro.image);
				await removeFileFTP(currentPage, oldRegistro.image);
			}

			body.image = req.file.filename;
			await addFileFTP(currentPage, body.image);
		}

		const newRegistro = await Ministry.findByIdAndUpdate(req.params.id, body, {
			new: true,
		});

		const registro = await Ministry.findById(newRegistro._id).populate(
			"_idCategory"
		);

		return res.send(registro);
	},

	async excluir(req, res) {
		const registro = await Ministry.findById(req.params.id);

		if (registro && registro.image) {
			await removeFile(registro.image);
			await removeFileFTP(currentPage, registro.image);
		}

		await Ministry.findByIdAndRemove(req.params.id);
		return res.send();
	},
};
