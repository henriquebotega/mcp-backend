const mongoose = require("mongoose");
const User = mongoose.model("User");

var jwt = require("jsonwebtoken");

module.exports = {
	async getAll(req, res) {
		const { pg = 0, qtd = 10 } = req.query;

		const registrosFiltrados = await User.find({})
			.limit(Number(qtd))
			.skip(Number(qtd) * Number(pg))
			.sort({ createdAt: -1 });

		const registros = await User.find({});

		return res.send({ items: registrosFiltrados, total: registros.length });
	},

	async getByID(req, res) {
		const registro = await User.findById(req.params.id);
		return res.send(registro);
	},

	async incluir(req, res) {
		try {
			const registro = await User.create(req.body);
			return res.send(registro);
		} catch (e) {
			return res.status(400).send({ error: "Este e-mail já esta cadastrado em outra conta." });
		}
	},

	async editar(req, res) {
		try {
			const registro = await User.findByIdAndUpdate(req.params.id, req.body, {
				new: true,
			});
			return res.send(registro);
		} catch (e) {
			return res.status(400).send({ error: "Este e-mail já esta cadastrado em outra conta." });
		}
	},

	async excluir(req, res) {
		await User.findByIdAndRemove(req.params.id);
		return res.send();
	},

	async login(req, res) {
		const item = await User.findOne({
			email: req.body.email,
			password: req.body.password,
		});

		if (item) {
			var token = jwt.sign({ item: { ...item._doc, password: undefined } }, "mcp-backend", {
				expiresIn: "120m", // 2h
			});

			return res.status(200).send({ auth: true, token: token });
		}

		return res.status(401).send({ error: "E-mail/Senha inválido!" });
	},

	async logout(req, res) {
		res.status(200).send({ auth: false, token: null });
	},
};
