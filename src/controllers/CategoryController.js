const mongoose = require("mongoose");
const Category = mongoose.model("Category");

const path = require("path");
const { addFileFTP, removeFileFTP, removeFile } = require("../config/jsftp");

const currentPage = "category";

module.exports = {
	async getAll(req, res) {
		const { pg = 0, qtd = 10 } = req.query;

		const registrosFiltrados = await Category.find({})
			.limit(Number(qtd))
			.skip(Number(qtd) * Number(pg))
			.sort({ position: 1 });

		const registros = await Category.find({});

		return res.send({ items: registrosFiltrados, total: registros.length });
	},

	async getByID(req, res) {
		const registro = await Category.findById(req.params.id);
		return res.send(registro);
	},

	async incluir(req, res) {
		const { position, title } = req.body;

		const body = {
			position,
			title,
		};

		if (req.file) {
			body.image = req.file.filename;
			await addFileFTP(currentPage, body.image);
		}

		const registro = await Category.create(body);
		return res.send(registro);
	},

	async editar(req, res) {
		const { position, title } = req.body;

		const body = {
			position,
			title,
		};

		if (req.file) {
			const oldRegistro = await Category.findById(req.params.id);

			if (oldRegistro && oldRegistro.image) {
				await removeFile(oldRegistro.image);
				await removeFileFTP(currentPage, oldRegistro.image);
			}

			body.image = req.file.filename;
			await addFileFTP(currentPage, body.image);
		}

		const registro = await Category.findByIdAndUpdate(req.params.id, body, {
			new: true,
		});

		return res.send(registro);
	},

	async excluir(req, res) {
		const registro = await Category.findById(req.params.id);

		if (registro && registro.image) {
			await removeFile(registro.image);
			await removeFileFTP(currentPage, registro.image);
		}

		await Category.findByIdAndRemove(req.params.id);
		return res.send();
	},
};
