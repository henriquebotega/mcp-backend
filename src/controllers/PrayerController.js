const mongoose = require("mongoose");
const sendEmail = require("../config/email");
const Prayer = mongoose.model("Prayer");

module.exports = {
	async getAll(req, res) {
		const { pg = 0, qtd = 10 } = req.query;

		const registrosFiltrados = await Prayer.find({})
			.limit(Number(qtd))
			.skip(Number(qtd) * Number(pg))
			.sort({ createdAt: -1 });

		const registros = await Prayer.find({});

		return res.send({ items: registrosFiltrados, total: registros.length });
	},

	async getByID(req, res) {
		const registro = await Prayer.findById(req.params.id);
		return res.send(registro);
	},

	async incluir(req, res) {
		const registro = await Prayer.create(req.body);

		const message = `
			<h1>Oração via MCP</h1>

			<ul>
				<li>Name: ${registro.name}</li>
				<li>E-mail: ${registro.email}</li>
				<li>Telefone: ${registro.phone}</li>
				<li>Mensagem: ${registro.message}</li>
			</ul>
		`;

		sendEmail("oracao@mulhercompalavra.com.br", "Oração via MCP", message);

		return res.send(registro);
	},

	async editar(req, res) {
		const registro = await Prayer.findByIdAndUpdate(req.params.id, req.body, {
			new: true,
		});
		return res.send(registro);
	},

	async excluir(req, res) {
		await Prayer.findByIdAndRemove(req.params.id);
		return res.send();
	},
};
