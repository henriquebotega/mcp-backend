const mongoose = require("mongoose");
const About = mongoose.model("About");

const path = require("path");
const { addFileFTP, removeFileFTP, removeFile } = require("../config/jsftp");

const currentPage = "about";

module.exports = {
	async getAll(req, res) {
		const { pg = 0, qtd = 10 } = req.query;

		const registrosFiltrados = await About.find({})
			.limit(Number(qtd))
			.skip(Number(qtd) * Number(pg))
			.sort({ position: 1 });

		const registros = await About.find({});

		return res.send({ items: registrosFiltrados, total: registros.length });
	},

	async getByID(req, res) {
		const registro = await About.findById(req.params.id);
		return res.send(registro);
	},

	async incluir(req, res) {
		const { position, title, description } = req.body;

		const body = {
			position,
			title,
			description,
		};

		if (req.file) {
			body.image = req.file.filename;
			await addFileFTP(currentPage, body.image);
		}

		const registro = await About.create(body);
		return res.send(registro);
	},

	async editar(req, res) {
		const { position, title, description } = req.body;

		const body = {
			position,
			title,
			description,
		};

		if (req.file) {
			const oldRegistro = await About.findById(req.params.id);

			if (oldRegistro && oldRegistro.image) {
				await removeFile(oldRegistro.image);
				await removeFileFTP(currentPage, oldRegistro.image);
			}

			body.image = req.file.filename;
			await addFileFTP(currentPage, body.image);
		}

		const registro = await About.findByIdAndUpdate(req.params.id, body, {
			new: true,
		});

		return res.send(registro);
	},

	async excluir(req, res) {
		const registro = await About.findById(req.params.id);

		if (registro && registro.image) {
			await removeFile(registro.image);
			await removeFileFTP(currentPage, registro.image);
		}

		await About.findByIdAndRemove(req.params.id);
		return res.send();
	},
};
