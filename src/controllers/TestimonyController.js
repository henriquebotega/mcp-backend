const mongoose = require("mongoose");
const Testimony = mongoose.model("Testimony");

const path = require("path");
const { addFileFTP, removeFileFTP, removeFile } = require("../config/jsftp");

const currentPage = "testimony";

const all = async (req, random = false) => {
	const { pg = 0, qtd = 10 } = req.query;

	let registrosFiltrados = [];

	if (random && qtd == 3) {
		const items = await Testimony.aggregate([
			{ $match: {} },
			{ $sample: { size: parseInt(qtd) } },
		]);

		await Promise.all(
			items.map(
				(r) =>
					new Promise(async (resolve, reject) =>
						resolve(await Testimony.findById(r._id))
					)
			)
		).then((res) => (registrosFiltrados = res));
	} else {
		registrosFiltrados = await Testimony.find({})
			.limit(Number(qtd))
			.skip(Number(qtd) * Number(pg))
			.sort({ createdAt: -1 });
	}

	const registros = await Testimony.find({});

	return { registrosFiltrados, registros };
};

module.exports = {
	async getAllWeb(req, res) {
		const { registrosFiltrados, registros } = await all(req, true);
		return res.send({ items: registrosFiltrados, total: registros.length });
	},

	async getAll(req, res) {
		const { registrosFiltrados, registros } = await all(req);
		return res.send({ items: registrosFiltrados, total: registros.length });
	},

	async getByID(req, res) {
		const registro = await Testimony.findById(req.params.id);
		return res.send(registro);
	},

	async incluir(req, res) {
		const { name, message } = req.body;

		const body = {
			name,
			message,
		};

		if (req.file) {
			body.image = req.file.filename;
			await addFileFTP(currentPage, body.image);
		}

		const registro = await Testimony.create(body);
		return res.send(registro);
	},

	async editar(req, res) {
		const { name, message } = req.body;

		const body = {
			name,
			message,
		};

		if (req.file) {
			const oldRegistro = await Testimony.findById(req.params.id);

			if (oldRegistro && oldRegistro.image) {
				await removeFile(oldRegistro.image);
				await removeFileFTP(currentPage, oldRegistro.image);
			}

			body.image = req.file.filename;
			await addFileFTP(currentPage, body.image);
		}

		const registro = await Testimony.findByIdAndUpdate(req.params.id, body, {
			new: true,
		});

		return res.send(registro);
	},

	async excluir(req, res) {
		const registro = await Testimony.findById(req.params.id);

		if (registro && registro.image) {
			await removeFile(registro.image);
			await removeFileFTP(currentPage, registro.image);
		}

		await Testimony.findByIdAndRemove(req.params.id);
		return res.send();
	},
};
