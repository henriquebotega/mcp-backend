const mongoose = require("mongoose");
const Event = mongoose.model("Event");

const path = require("path");
const { addFileFTP, removeFileFTP, removeFile } = require("../config/jsftp");

const currentPage = "event";

const all = async (req, active = false) => {
	const { pg = 0, qtd = 10 } = req.query;

	const filtro = active ? { active: true } : {};

	const registrosFiltrados = await Event.find(filtro)
		.limit(Number(qtd))
		.skip(Number(qtd) * Number(pg))
		.sort({ position: 1 });

	const registros = await Event.find(filtro);

	return { registrosFiltrados, registros };
};

module.exports = {
	async getAllWeb(req, res) {
		const { registrosFiltrados, registros } = await all(req, true);
		return res.send({ items: registrosFiltrados, total: registros.length });
	},

	async getAll(req, res) {
		const { registrosFiltrados, registros } = await all(req);
		return res.send({ items: registrosFiltrados, total: registros.length });
	},

	async getByID(req, res) {
		const registro = await Event.findById(req.params.id);
		return res.send(registro);
	},

	async incluir(req, res) {
		const {
			position,
			title,
			description,
			active,
			value,
			start_date,
			end_date,
			link,
			type,
		} = req.body;

		const body = {
			position,
			title,
			description,
			active,
			value,
			start_date,
			end_date,
			link,
			type,
		};

		if (req.file) {
			body.image = req.file.filename;
			await addFileFTP(currentPage, body.image);
		}

		const registro = await Event.create(body);
		return res.send(registro);
	},

	async editar(req, res) {
		const {
			position,
			title,
			description,
			active,
			value,
			start_date,
			end_date,
			link,
			type,
		} = req.body;

		const body = {
			position,
			title,
			description,
			active,
			value,
			start_date,
			end_date,
			link,
			type,
		};

		if (req.file) {
			const oldRegistro = await Event.findById(req.params.id);

			if (oldRegistro && oldRegistro.image) {
				await removeFile(oldRegistro.image);
				await removeFileFTP(currentPage, oldRegistro.image);
			}

			body.image = req.file.filename;
			await addFileFTP(currentPage, body.image);
		}

		const registro = await Event.findByIdAndUpdate(req.params.id, body, {
			new: true,
		});

		return res.send(registro);
	},

	async excluir(req, res) {
		const registro = await Event.findById(req.params.id);

		if (registro && registro.image) {
			await removeFile(registro.image);
			await removeFileFTP(currentPage, registro.image);
		}

		await Event.findByIdAndRemove(req.params.id);
		return res.send();
	},
};
