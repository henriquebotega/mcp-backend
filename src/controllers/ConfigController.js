const mongoose = require("mongoose");
const Config = mongoose.model("Config");

module.exports = {
	async getAll(req, res) {
		const { pg = 0, qtd = 10 } = req.query;

		const registrosFiltrados = await Config.find({})
			.limit(Number(qtd))
			.skip(Number(qtd) * Number(pg))
			.sort({ createdAt: -1 });

		const registros = await Config.find({});

		return res.send({ items: registrosFiltrados, total: registros.length });
	},

	async getByID(req, res) {
		const registro = await Config.findById(req.params.id);
		return res.send(registro);
	},

	async incluir(req, res) {
		const registro = await Config.create(req.body);
		return res.send(registro);
	},

	async editar(req, res) {
		const registro = await Config.findByIdAndUpdate(req.params.id, req.body, {
			new: true,
		});
		return res.send(registro);
	},

	async excluir(req, res) {
		await Config.findByIdAndRemove(req.params.id);
		return res.send();
	},
};
