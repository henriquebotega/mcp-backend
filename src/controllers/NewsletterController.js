const mongoose = require("mongoose");
const Newsletter = mongoose.model("Newsletter");

module.exports = {
	async getAll(req, res) {
		const { pg = 0, qtd = 10 } = req.query;

		const registrosFiltrados = await Newsletter.find({})
			.limit(Number(qtd))
			.skip(Number(qtd) * Number(pg))
			.sort({ createdAt: -1 });

		const registros = await Newsletter.find({});

		return res.send({ items: registrosFiltrados, total: registros.length });
	},

	async getByID(req, res) {
		const registro = await Newsletter.findById(req.params.id);
		return res.send(registro);
	},

	async incluir(req, res) {
		try {
			const registro = await Newsletter.create(req.body);
			return res.send(registro);
		} catch (e) {
			return res
				.status(404)
				.json({ error: "Este e-mail já esta cadastrado em outra conta." });
		}
	},

	async editar(req, res) {
		try {
			const registro = await Newsletter.findByIdAndUpdate(
				req.params.id,
				req.body,
				{
					new: true,
				}
			);
			return res.send(registro);
		} catch (e) {
			return res
				.status(404)
				.json({ error: "Este e-mail já esta cadastrado em outra conta." });
		}
	},

	async excluir(req, res) {
		await Newsletter.findByIdAndRemove(req.params.id);
		return res.send();
	},
};
