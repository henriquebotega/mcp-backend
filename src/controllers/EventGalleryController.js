const mongoose = require("mongoose");
const EventGallery = mongoose.model("EventGallery");

const path = require("path");
const { addFileFTP, removeFileFTP, removeFile } = require("../config/jsftp");

const currentPage = "event_gallery";

module.exports = {
	async getAll(req, res) {
		const { pg = 0, qtd = 10 } = req.query;
		const _idEvent = req.params.event_id;

		const registrosFiltrados = await EventGallery.find({ _idEvent })
			.limit(Number(qtd))
			.skip(Number(qtd) * Number(pg))
			.sort({ createdAt: -1 })
			.populate("_idEvent");

		const registros = await EventGallery.find({});
		return res.send({ items: registrosFiltrados, total: registros.length });
	},

	async getByID(req, res) {
		const _idEvent = req.params.event_id;

		const registro = await EventGallery.find({
			_id: req.params.id,
			_idEvent,
		}).populate("_idEvent");

		return res.send(registro[0]);
	},

	async incluir(req, res) {
		const _idEvent = req.params.event_id;

		const body = {
			_idEvent,
		};

		if (req.file) {
			body.image = req.file.filename;
			await addFileFTP(currentPage, body.image);
		}

		const newRegistro = await EventGallery.create(body);
		const registro = await EventGallery.findById(newRegistro._id).populate(
			"_idEvent"
		);

		return res.send(registro);
	},

	async editar(req, res) {
		const _idEvent = req.params.event_id;

		const body = {
			_idEvent,
		};

		if (req.file) {
			const oldRegistro = await EventGallery.find({
				_id: req.params.id,
				_idEvent,
			});

			if (oldRegistro[0].image) {
				await removeFile(oldRegistro[0].image);
				await removeFileFTP(currentPage, oldRegistro[0].image);
			}

			body.image = req.file.filename;
			await addFileFTP(currentPage, body.image);
		}

		const newRegistro = await EventGallery.findByIdAndUpdate(
			req.params.id,
			body,
			{
				new: true,
			}
		);

		const registro = await EventGallery.findById(newRegistro._id).populate(
			"_idEvent"
		);

		return res.send(registro);
	},

	async excluir(req, res) {
		const registro = await EventGallery.find({
			_id: req.params.id,
			_idEvent: req.params.event_id,
		});

		if (registro[0].image) {
			await removeFile(registro[0].image);
			await removeFileFTP(currentPage, registro[0].image);
		}

		await EventGallery.findByIdAndRemove(req.params.id);
		return res.send();
	},
};
