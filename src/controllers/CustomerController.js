const mongoose = require("mongoose");
const Customer = mongoose.model("Customer");

var jwt = require("jsonwebtoken");
const { sendEmail, encode_utf8, decode_utf8, avisarSobreErro } = require("../util");

module.exports = {
	async getAll(req, res) {
		const { pg = 0, qtd = 10 } = req.query;

		const registrosFiltrados = await Customer.find({})
			.limit(Number(qtd))
			.skip(Number(qtd) * Number(pg))
			.sort({ createdAt: -1 });

		const registros = await Customer.find({});

		return res.send({ items: registrosFiltrados, total: registros.length });
	},

	async getByID(req, res) {
		const registro = await Customer.findById(req.params.id);
		return res.send(registro);
	},

	async incluir(req, res) {
		try {
			const registro = await Customer.create(req.body);

			const subject = "Cadastro realizado com sucesso - Mulher com Palavra";
			const html = `
			<html>
				<body>
					<h1>Seja bem vindo ao Mulher com Palavra,</h1>

					Seu cadastro foi realizado com sucesso.<br />

					<p>Atenciosamente, Equipe Mulher com Palavra.</p>
				</body>
			</html>`;

			await sendEmail({ to: `${registro.email}`, subject, html });

			return res.send(registro);
		} catch (e) {
			avisarSobreErro(req.body, e);
			return res.status(400).send({ error: "Este e-mail já esta cadastrado em outra conta." });
		}
	},

	async editar(req, res) {
		try {
			const registro = await Customer.findByIdAndUpdate(req.params.id, req.body, {
				new: true,
			});
			return res.send(registro);
		} catch (e) {
			avisarSobreErro(req.body, e);
			return res.status(400).send({ error: "Este e-mail já esta cadastrado em outra conta." });
		}
	},

	async excluir(req, res) {
		await Customer.findByIdAndRemove(req.params.id);
		return res.send();
	},

	async loginIsValid(req, res) {
		return res.send();
	},

	async login(req, res) {
		const item = await Customer.findOne({
			email: req.body.email,
			password: req.body.password,
		});

		if (item) {
			// var token = jwt.sign({ item: { ...item._doc, password: undefined } }, "mcp-backend-customer", {
			// 	expiresIn: "120m", // 2h
			// });

			var token = jwt.sign({ item: { ...item._doc } }, "mcp-backend-customer", {
				expiresIn: "120m", // 2h
			});

			return res.status(200).send({ auth: true, token: token });
		}

		return res.status(401).send({ error: "E-mail/Senha inválido!" });
	},

	async logout(req, res) {
		res.status(200).send({ auth: false, token: null });
	},
};
