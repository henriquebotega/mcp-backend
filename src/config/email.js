const nodemailer = require("nodemailer");

const smtpPort = 465;

const transporter = nodemailer.createTransport({
	host: "email-ssl.com.br",
	port: smtpPort,
	secure: smtpPort === 465 ? true : false,
	auth: {
		user: "naoresponda@mulhercompalavra.com.br",
		pass: "N@0r3sp0nd4",
	},
});

const sendEmail = async (to, subject, html) => {
	await transporter.sendMail({
		from: '"Website MCP" <naoresponda@mulhercompalavra.com.br>',
		to,
		subject,
		html,
	});
};

module.exports = sendEmail;
