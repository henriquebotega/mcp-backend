const multer = require("multer");
const path = require("path");
const crypto = require("crypto");
const fs = require("fs");

module.exports = {
	dest: "/tmp/",
	storage: multer.diskStorage({
		destination: (req, file, cb) => cb(null, "/tmp/"),
		filename: (req, file, cb) => {
			crypto.randomBytes(16, (err, hash) => {
				file.key = hash.toString("hex") + path.extname(file.originalname);
				cb(null, file.key);
			});
		},
	}),
};
