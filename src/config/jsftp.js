// const jsftp = require("jsftp");
const fs = require("fs");
const { promisify } = require("util");
const unlinkAsync = promisify(fs.unlink);
const ftp = require("basic-ftp")

const addFileFTP = async (currentPage, fileName) =>
{
	const arquivo = "/tmp/" + fileName;

	const client = new ftp.Client()
	client.ftp.verbose = true

    try {
        await client.access({
            host: process.env.FTP_HOST,
            user: process.env.FTP_USER,
            password: process.env.FTP_PASSWORD,
        })

        await client.uploadFrom(arquivo, `/public_html/files/${currentPage}/${fileName}`)
    } catch(err) {
        console.log('FTP error (addFileFTP): ', err)
	} finally {
        client.close()
    }

	// Remove o arquivo local, apos enviar ao FTP
	await removeFile(arquivo);
};

const removeFileFTP = async (currentPage, file) => {
	const client = new ftp.Client()
	client.ftp.verbose = true

    try {
        await client.access({
            host: process.env.FTP_HOST,
            user: process.env.FTP_USER,
            password: process.env.FTP_PASSWORD,
        })

        await client.remove(`/public_html/files/${currentPage}/${file}`)
    } catch(err) {
        console.log('FTP error (removeFileFTP): ', err)
    } finally {
        client.close()
    }
};

const removeFile = async (file) => {
	try {
		// Remove o arquivo local, apos enviar ao FTP
		await unlinkAsync("/tmp/" + file);
	} catch (e) {}
};

module.exports = { addFileFTP, removeFileFTP, removeFile };
