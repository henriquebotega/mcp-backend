const express = require("express");
const routes = express.Router();
const jwt = require("jsonwebtoken");
const path = require("path");
const fs = require("fs");

const multer = require("multer");
const multerConfig = require("./config/multer");

const checkJWT = (req, res, next) => {
	var token = req.headers["x-access-token"];

	if (!token) {
		return res.status(401).send({ auth: false, message: "No token provided" });
	}

	jwt.verify(token, "mcp-backend", (error, decoded) => {
		if (error) {
			if (error.message.indexOf("jwt expired") > -1) {
				return res
					.status(401)
					.send({ auth: false, message: "No token provided" });
			} else {
				return res
					.status(500)
					.send({ auth: false, message: "Failed to authenticate token" });
			}
		}

		// req.user = decoded.item;
		next();
	});
};

const checkJWTCustomer = (req, res, next) => {
	var token = req.headers["x-access-token"];

	if (!token) {
		return res.status(401).send({ auth: false, message: "No token provided" });
	}

	jwt.verify(token, "mcp-backend-customer", (error, decoded) => {
		if (error) {
			if (error.message.indexOf("jwt expired") > -1) {
				return res
					.status(401)
					.send({ auth: false, message: "No token provided" });
			} else {
				return res
					.status(500)
					.send({ auth: false, message: "Failed to authenticate token" });
			}
		}

		req.customer = decoded.item;
		next();
	});
};
/******************/
/* UserController */
/******************/
const UserController = require("./controllers/UserController");
routes.get("/user", checkJWT, UserController.getAll);
routes.get("/user/:id", checkJWT, UserController.getByID);
routes.post("/user", checkJWT, UserController.incluir);
routes.put("/user/:id", checkJWT, UserController.editar);
routes.delete("/user/:id", checkJWT, UserController.excluir);

/************************/
/* NewsletterController */
/************************/
const NewsletterController = require("./controllers/NewsletterController");
routes.get("/newsletter", checkJWT, NewsletterController.getAll);
routes.get("/newsletter/:id", checkJWT, NewsletterController.getByID);
routes.post("/newsletter", checkJWT, NewsletterController.incluir);
routes.put("/newsletter/:id", checkJWT, NewsletterController.editar);
routes.delete("/newsletter/:id", checkJWT, NewsletterController.excluir);

/********************/
/* PrayerController */
/********************/
const PrayerController = require("./controllers/PrayerController");
routes.get("/prayer", checkJWT, PrayerController.getAll);
routes.get("/prayer/:id", checkJWT, PrayerController.getByID);
routes.post("/prayer", checkJWT, PrayerController.incluir);
routes.put("/prayer/:id", checkJWT, PrayerController.editar);
routes.delete("/prayer/:id", checkJWT, PrayerController.excluir);

/********************/
/* TestimonyController */
/********************/
const TestimonyController = require("./controllers/TestimonyController");
routes.get("/testimony", checkJWT, TestimonyController.getAll);
routes.get("/testimony/:id", checkJWT, TestimonyController.getByID);
routes.post(
	"/testimony",
	checkJWT,
	multer(multerConfig).single("file"),
	TestimonyController.incluir
);
routes.put(
	"/testimony/:id",
	checkJWT,
	multer(multerConfig).single("file"),
	TestimonyController.editar
);
routes.delete("/testimony/:id", checkJWT, TestimonyController.excluir);

/********************/
/* BannerController */
/********************/
const BannerController = require("./controllers/BannerController");
routes.get("/banner", checkJWT, BannerController.getAll);
routes.get("/banner/:id", checkJWT, BannerController.getByID);
routes.post(
	"/banner",
	checkJWT,
	multer(multerConfig).single("file"),
	BannerController.incluir
);
routes.put(
	"/banner/:id",
	checkJWT,
	multer(multerConfig).single("file"),
	BannerController.editar
);
routes.delete("/banner/:id", checkJWT, BannerController.excluir);

/*******************/
/* AboutController */
/*******************/
const AboutController = require("./controllers/AboutController");
routes.get("/about", checkJWT, AboutController.getAll);
routes.get("/about/:id", checkJWT, AboutController.getByID);
routes.post(
	"/about",
	checkJWT,
	multer(multerConfig).single("file"),
	AboutController.incluir
);
routes.put(
	"/about/:id",
	checkJWT,
	multer(multerConfig).single("file"),
	AboutController.editar
);
routes.delete("/about/:id", checkJWT, AboutController.excluir);

/******************/
/* TeamController */
/******************/
const TeamController = require("./controllers/TeamController");
routes.get("/team", checkJWT, TeamController.getAll);
routes.get("/team/:id", checkJWT, TeamController.getByID);
routes.post(
	"/team",
	checkJWT,
	multer(multerConfig).single("file"),
	TeamController.incluir
);
routes.put(
	"/team/:id",
	checkJWT,
	multer(multerConfig).single("file"),
	TeamController.editar
);
routes.delete("/team/:id", checkJWT, TeamController.excluir);

/**********************/
/* MinistryController */
/**********************/
const MinistryController = require("./controllers/MinistryController");
routes.get("/ministry", checkJWT, MinistryController.getAll);
routes.get("/ministry/:id", checkJWT, MinistryController.getByID);
routes.post(
	"/ministry",
	checkJWT,
	multer(multerConfig).single("file"),
	MinistryController.incluir
);
routes.put(
	"/ministry/:id",
	checkJWT,
	multer(multerConfig).single("file"),
	MinistryController.editar
);
routes.delete("/ministry/:id", checkJWT, MinistryController.excluir);
routes.get(
	"/ministry/category/:category_id",
	checkJWT,
	MinistryController.getAllWebByCategory
);

/**********************/
/* CategoryController */
/**********************/
const CategoryController = require("./controllers/CategoryController");
routes.get("/category", checkJWT, CategoryController.getAll);
routes.get("/category/:id", checkJWT, CategoryController.getByID);
routes.post(
	"/category",
	checkJWT,
	multer(multerConfig).single("file"),
	CategoryController.incluir
);
routes.put(
	"/category/:id",
	checkJWT,
	multer(multerConfig).single("file"),
	CategoryController.editar
);
routes.delete("/category/:id", checkJWT, CategoryController.excluir);

/**********************/
/* ContactController */
/**********************/
const ContactController = require("./controllers/ContactController");
routes.get("/contact", checkJWT, ContactController.getAll);
routes.get("/contact/:id", checkJWT, ContactController.getByID);
routes.post("/contact", checkJWT, ContactController.incluir);
routes.put("/contact/:id", checkJWT, ContactController.editar);
routes.delete("/contact/:id", checkJWT, ContactController.excluir);

/**********************/
/* CustomerController */
/**********************/
const CustomerController = require("./controllers/CustomerController");
routes.get("/customer", checkJWT, CustomerController.getAll);
routes.get("/customer/:id", checkJWT, CustomerController.getByID);
routes.post("/customer", checkJWT, CustomerController.incluir);
routes.put("/customer/:id", checkJWT, CustomerController.editar);
routes.delete("/customer/:id", checkJWT, CustomerController.excluir);

/*************************/
/* OrderStatusController */
/*************************/
const OrderStatusController = require("./controllers/OrderStatusController");
routes.get("/order_status", checkJWT, OrderStatusController.getAll);
routes.get("/order_status/:id", checkJWT, OrderStatusController.getByID);
routes.post("/order_status", checkJWT, OrderStatusController.incluir);
routes.put("/order_status/:id", checkJWT, OrderStatusController.editar);
routes.delete("/order_status/:id", checkJWT, OrderStatusController.excluir);

/*************************/
/* OrderProductController */
/*************************/
const OrderProductController = require("./controllers/OrderProductController");
routes.get("/order_product/:order_id", checkJWT, OrderProductController.getAll);
routes.get(
	"/order_product/:order_id/:id",
	checkJWT,
	OrderProductController.getByID
);
routes.post(
	"/order_product/:order_id",
	checkJWT,
	OrderProductController.incluir
);
routes.put(
	"/order_product/:order_id/:id",
	checkJWT,
	OrderProductController.editar
);
routes.delete(
	"/order_product/:order_id/:id",
	checkJWT,
	OrderProductController.excluir
);

/*************************/
/* OrderController */
/*************************/
const OrderController = require("./controllers/OrderController");
routes.get("/order", checkJWT, OrderController.getAll);
routes.get("/order/:id", checkJWT, OrderController.getByID);
routes.post("/order", checkJWT, OrderController.incluir);
routes.put("/order/:id", checkJWT, OrderController.editar);
routes.delete("/order/:id", checkJWT, OrderController.excluir);
routes.get(
	"/order/customer/:customer_id",
	checkJWT,
	OrderController.getAllByCustomer
);
routes.get(
	"/order/product/:product_id",
	checkJWT,
	OrderController.getAllByProduct
);

/**********************/
/* ConfigController */
/**********************/
const ConfigController = require("./controllers/ConfigController");
routes.get("/config", checkJWT, ConfigController.getAll);
routes.get("/config/:id", checkJWT, ConfigController.getByID);
routes.post("/config", checkJWT, ConfigController.incluir);
routes.put("/config/:id", checkJWT, ConfigController.editar);
routes.delete("/config/:id", checkJWT, ConfigController.excluir);

/**********************/
/* ProductController */
/**********************/
const ProductController = require("./controllers/ProductController");
routes.get("/product", checkJWT, ProductController.getAll);
routes.get("/product/:id", checkJWT, ProductController.getByID);
routes.post(
	"/product",
	checkJWT,
	multer(multerConfig).fields([{ name: "file" }, { name: "file_download" }]),
	ProductController.incluir
);
routes.put(
	"/product/:id",
	checkJWT,
	multer(multerConfig).fields([{ name: "file" }, { name: "file_download" }]),
	ProductController.editar
);
routes.delete("/product/:id", checkJWT, ProductController.excluir);

/**********************/
/* EventController */
/**********************/
const EventController = require("./controllers/EventController");
routes.get("/event", checkJWT, EventController.getAll);
routes.get("/event/:id", checkJWT, EventController.getByID);
routes.post(
	"/event",
	checkJWT,
	multer(multerConfig).single("file"),
	EventController.incluir
);
routes.put(
	"/event/:id",
	checkJWT,
	multer(multerConfig).single("file"),
	EventController.editar
);
routes.delete("/event/:id", checkJWT, EventController.excluir);

/**************************/
/* EventGalleryController */
/**************************/
const EventGalleryController = require("./controllers/EventGalleryController");
routes.get("/event_gallery/:event_id", checkJWT, EventGalleryController.getAll);
routes.get(
	"/event_gallery/:event_id/:id",
	checkJWT,
	EventGalleryController.getByID
);
routes.post(
	"/event_gallery/:event_id",
	checkJWT,
	multer(multerConfig).single("file"),
	EventGalleryController.incluir
);
routes.put(
	"/event_gallery/:event_id/:id",
	checkJWT,
	multer(multerConfig).single("file"),
	EventGalleryController.editar
);
routes.delete(
	"/event_gallery/:event_id/:id",
	checkJWT,
	EventGalleryController.excluir
);

/******************/
/* Extra - Website*/
/******************/
routes.post("/login", UserController.login);
routes.get("/logout", UserController.logout);

routes.get("/web/banner", BannerController.getAllWeb);
routes.get("/web/testimony", TestimonyController.getAllWeb);
routes.get("/web/about", AboutController.getAll);
routes.get("/web/team", TeamController.getAll);
routes.post("/web/ministry/withfilter", MinistryController.postFilter);
routes.get("/web/ministry", MinistryController.getAllWeb);
routes.get("/web/ministry/:id", MinistryController.getByID);
routes.get(
	"/web/ministry/category/:category_id",
	MinistryController.getAllByCategory
);
routes.get("/web/category", CategoryController.getAll);
routes.get("/web/product", ProductController.getAllWeb);
routes.get("/web/product/:id", ProductController.getByID);
routes.get("/web/event", EventController.getAllWeb);
routes.get("/web/event/:id", EventController.getByID);
routes.get("/web/event_gallery/:event_id", EventGalleryController.getAll);
routes.post("/web/prayer", PrayerController.incluir);
routes.post("/web/contact", ContactController.incluir);
routes.post("/web/newsletter", NewsletterController.incluir);
routes.post("/web/customer", CustomerController.incluir);
routes.put("/web/customer/:id", CustomerController.editar);

const PagSeguroController = require("./controllers/PagSeguroController");
routes.get("/web/pagseguro/session", PagSeguroController.getSession);
routes.post("/web/pagseguro/pedido", PagSeguroController.setPedido);
routes.post("/web/pagseguro/frete", PagSeguroController.calcularFrete);
routes.post("/web/pagseguro/notificacao", PagSeguroController.notificacao);
routes.post("/web/pagseguro/cancelar", PagSeguroController.cancelarPedido);
routes.post("/web/pagseguro/estornar", PagSeguroController.estornarPedido);

/* Extra - Customer */
routes.post("/web/login", CustomerController.login);
routes.get(
	"/web/loginIsValid",
	checkJWTCustomer,
	CustomerController.loginIsValid
);
routes.get("/web/logout", CustomerController.logout);

routes.get("/web/customer/order", checkJWTCustomer, OrderController.getAllWeb);
routes.get(
	"/web/customer/order/:id",
	checkJWTCustomer,
	OrderController.getByIDByCustomer
);
routes.get(
	"/web/customer/order/cancel/:id",
	checkJWTCustomer,
	OrderController.cancelByIDByCustomer
);

routes.get(
	"/web/customer/order_product/:order_id",
	checkJWTCustomer,
	OrderProductController.getAllWeb
);

routes.get("/", (req, res) => {
	return res.json({ msg: "On-line" });
});

module.exports = routes;
