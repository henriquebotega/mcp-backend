const nodemailer = require("nodemailer");

const converterData = (vlr) => {
	const rec = vlr.split("-");
	return rec[2] + "/" + rec[1] + "/" + rec[0];
};

const encode_utf8 = (s) => {
	return unescape(encodeURIComponent(s));
};

const decode_utf8 = (s) => {
	return decodeURIComponent(escape(s));
};

async function avisarSobreErro(conteudo, e) {
	await sendEmail({ to: "henriquebotega@gmail.com", subject: "Ocorreu um erro - MCP", html: JSON.stringify(conteudo) + "<hr />" + JSON.stringify(e.message) });
}

async function sendEmail({ to, subject, html }) {
	let transporter = nodemailer.createTransport({
		host: "email-ssl.com.br",
		port: 465,
		secure: true,
		auth: {
			user: "contato@mulhercompalavra.com.br",
			pass: "Mp@2020_",
		},
		debug: false,
		logger: false,
	});

	try {
		await transporter.sendMail({
			from: '"Mulher com Palavra" <contato@mulhercompalavra.com.br>',
			to,
			subject,
			html,
		});
	} catch {}
}

module.exports = { sendEmail, avisarSobreErro, converterData, encode_utf8, decode_utf8 };
