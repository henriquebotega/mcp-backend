const mongoose = require("mongoose");

const TeamSchema = new mongoose.Schema(
	{
		position: { type: Number, default: 1 },
		title: { type: String, required: true },
		description: { type: String, required: true },
		image: { type: String, required: true },
	},
	{ timestamps: true, toObject: { virtuals: true }, toJSON: { virtuals: true } }
);

TeamSchema.virtual("url").get(function () {
	return process.env.URL_FILES + "/team/" + encodeURIComponent(this.image);
});

module.exports = mongoose.model("Team", TeamSchema);
