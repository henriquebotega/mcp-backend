const mongoose = require("mongoose");

const EventGallerySchema = new mongoose.Schema(
	{
		image: { type: String, required: true },
		_idEvent: {
			type: mongoose.Schema.Types.ObjectId,
			ref: "Event",
		},
	},
	{ timestamps: true, toObject: { virtuals: true }, toJSON: { virtuals: true } }
);

EventGallerySchema.virtual("url").get(function () {
	return (url =
		process.env.URL_FILES + "/event_gallery/" + encodeURIComponent(this.image));
});

module.exports = mongoose.model("EventGallery", EventGallerySchema);
