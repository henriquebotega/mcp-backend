const mongoose = require("mongoose");

const EventSchema = new mongoose.Schema(
	{
		position: { type: Number, default: 1 },
		title: { type: String, required: true },
		description: { type: String, required: true },
		active: { type: Boolean, default: true },
		image: { type: String, required: true },

		value: { type: Number, default: 0 },
		start_date: { type: String },
		end_date: { type: String },
		link: { type: String },
		type: { type: String },
	},
	{ timestamps: true, toObject: { virtuals: true }, toJSON: { virtuals: true } }
);

EventSchema.virtual("url").get(function () {
	return (url =
		process.env.URL_FILES + "/event/" + encodeURIComponent(this.image));
});

module.exports = mongoose.model("Event", EventSchema);
