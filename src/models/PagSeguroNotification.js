const mongoose = require("mongoose");

const PagSeguroNotificationSchema = new mongoose.Schema(
	{
		notificationCode: { type: String },
	},
	{ timestamps: true }
);

module.exports = mongoose.model(
	"PagSeguroNotification",
	PagSeguroNotificationSchema
);
