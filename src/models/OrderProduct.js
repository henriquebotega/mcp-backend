const mongoose = require("mongoose");

const OrderProductSchema = new mongoose.Schema(
	{
		qtd: { type: Number, required: true, default: 1 },
		unit: { type: Number, required: true },
		_idProduct: {
			type: mongoose.Schema.Types.ObjectId,
			ref: "Product",
		},
		_idOrder: {
			type: mongoose.Schema.Types.ObjectId,
			ref: "Order",
		},
		total: { type: Number, required: true },
	},
	{ timestamps: true }
);

module.exports = mongoose.model("OrderProduct", OrderProductSchema);
