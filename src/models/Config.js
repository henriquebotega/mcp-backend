const mongoose = require("mongoose");

const ConfigSchema = new mongoose.Schema(
	{
		email_pagseguro: { type: String, required: true },
		token_pagseguro: { type: String, required: true },
		cep_origem_frete: { type: String, required: true },
	},
	{ timestamps: true }
);

module.exports = mongoose.model("Config", ConfigSchema);
