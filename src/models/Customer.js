const mongoose = require("mongoose");

const CustomerSchema = new mongoose.Schema(
	{
		name: { type: String, required: true },
		email: { type: String, required: true, unique: true },
		password: { type: String, required: true },
		cpf: { type: String },
		phone: { type: String },
		dtNasc: { type: String },
		address_postal_code: { type: String },
		address_street: { type: String },
		address_number: { type: String },
		address_complement: { type: String },
		address_district: { type: String },
		address_city: { type: String },
		address_state: { type: String },
	},
	{ timestamps: true }
);

module.exports = mongoose.model("Customer", CustomerSchema);
