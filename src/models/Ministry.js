const mongoose = require("mongoose");

const MinistrySchema = new mongoose.Schema(
	{
		title: { type: String, required: true },
		description: { type: String, required: true },
		_idCategory: {
			type: mongoose.Schema.Types.ObjectId,
			ref: "Category",
		},
		active: { type: Boolean, default: true },
		video: { type: String },
		image: { type: String },
	},
	{ timestamps: true, toObject: { virtuals: true }, toJSON: { virtuals: true } }
);

MinistrySchema.virtual("url").get(function () {
	return process.env.URL_FILES + "/ministry/" + encodeURIComponent(this.image);
});

module.exports = mongoose.model("Ministry", MinistrySchema);
