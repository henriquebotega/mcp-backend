const mongoose = require("mongoose");

const OrderSchema = new mongoose.Schema(
	{
		_idCustomer: {
			type: mongoose.Schema.Types.ObjectId,
			ref: "Customer",
			required: true,
		},
		_idOrderStatus: {
			type: mongoose.Schema.Types.ObjectId,
			ref: "OrderStatus",
			required: true,
		},
		reference: { type: String, required: true },
		frete: { type: Number, default: 0 },
		address_postal_code: { type: String },
		address_street: { type: String },
		address_number: { type: String },
		address_complement: { type: String },
		address_district: { type: String },
		address_city: { type: String },
		address_state: { type: String },
		address_email: { type: String },
	},
	{ timestamps: true }
);

module.exports = mongoose.model("Order", OrderSchema);
