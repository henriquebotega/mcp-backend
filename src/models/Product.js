const mongoose = require("mongoose");

const ProductSchema = new mongoose.Schema(
	{
		title: { type: String, required: true },
		description: { type: String },
		value: { type: Number, default: 0 },
		image: { type: String, required: true },
		download: { type: String },
		active: { type: Boolean, default: true },
		is_virtual: { type: Boolean, default: false },
		disable_shopping_cart: { type: Boolean, default: true },
		width: { type: Number, default: 0 },
		height: { type: Number, default: 0 },
		length: { type: Number, default: 0 },
		weight: { type: Number, default: 0 },
	},
	{ timestamps: true, toObject: { virtuals: true }, toJSON: { virtuals: true } }
);

ProductSchema.virtual("url").get(function () {
	return (url = process.env.URL_FILES + "/product/" + encodeURIComponent(this.image));
});

ProductSchema.virtual("url_download").get(function () {
	return (url_download = process.env.URL_FILES + "/product/" + encodeURIComponent(this.download));
});

module.exports = mongoose.model("Product", ProductSchema);
