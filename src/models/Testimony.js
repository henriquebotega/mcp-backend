const mongoose = require("mongoose");

const TestimonySchema = new mongoose.Schema(
	{
		name: { type: String, required: true },
		image: { type: String, required: true },
		message: { type: String, required: true },
	},
	{ timestamps: true, toObject: { virtuals: true }, toJSON: { virtuals: true } }
);

TestimonySchema.virtual("url").get(function () {
	return (url =
		process.env.URL_FILES + "/testimony/" + encodeURIComponent(this.image));
});

module.exports = mongoose.model("Testimony", TestimonySchema);
