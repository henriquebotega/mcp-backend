const mongoose = require("mongoose");

const BannerSchema = new mongoose.Schema(
	{
		position: { type: Number, default: 1 },
		title: { type: String, required: true },
		image: { type: String, required: true },
		active: { type: Boolean, default: true },
		link: String,
	},
	{ timestamps: true, toObject: { virtuals: true }, toJSON: { virtuals: true } }
);

BannerSchema.virtual("url").get(function () {
	return (url =
		process.env.URL_FILES + "/banner/" + encodeURIComponent(this.image));
});

module.exports = mongoose.model("Banner", BannerSchema);
