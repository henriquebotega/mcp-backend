const mongoose = require("mongoose");

const OrderStatusSchema = new mongoose.Schema(
	{
		code: { type: Number, required: true },
		title: { type: String, required: true },
	},
	{ timestamps: true }
);

module.exports = mongoose.model("OrderStatus", OrderStatusSchema);
