const mongoose = require("mongoose");

const CategorySchema = new mongoose.Schema(
	{
		position: { type: Number, default: 1 },
		title: { type: String, required: true },
		image: { type: String, required: true },
	},
	{ timestamps: true, toObject: { virtuals: true }, toJSON: { virtuals: true } }
);

CategorySchema.virtual("url").get(function () {
	return (url =
		process.env.URL_FILES + "/category/" + encodeURIComponent(this.image));
});

module.exports = mongoose.model("Category", CategorySchema);
